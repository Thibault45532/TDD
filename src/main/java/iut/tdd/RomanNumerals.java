package iut.tdd;

public class RomanNumerals {
	public String convertToRoman(String arabe) {
		int n=Integer.parseInt(arabe);
		String reponse="";
		while(n>=1000){
			reponse+="M";
			n-=1000;
		}
		while(n>=900){
			reponse+="CM";
			n-=900;
		}
		while(n>=500){
			reponse+="D";
			n-=500;
		}
		while(n>=400){
			reponse+="CD";
			n-=400;
		}
		while(n>=100){
			reponse+="C";
			n-=100;
		}
		while(n>=90){
			reponse+="XC";
			n-=90;
		}
		while(n>=50){
			reponse+="L";
			n-=50;
		}
		while(n>=40){
			reponse+="XL";
			n-=40;
		}
		while(n>=10){
			reponse+="X";
			n-=10;
		}
		while(n>=9){
			reponse+="IX";
			n-=9;
		}
		while(n>=5){
			reponse+="V";
			n-=5;
		}
		while(n>=4){
			reponse+="IV";
			n-=4;
		}
		while(n>=1){
			reponse+="I";
			n-=1;
		}
		System.out.println(reponse);

		return reponse;
	}

	public String convertFromRoman(String roman) {
		int reponse=0;
		for(int i=0;i<roman.length();i++){
			if(roman.charAt(i)=='M'){
				reponse+=1000;
			}else if(roman.charAt(i)=='D'){
				reponse+=500;
			}else if(roman.charAt(i)=='C'){
				reponse+=100;
			}else if(roman.charAt(i)=='L'){
				reponse+=50;
			}else if(roman.charAt(i)=='X'){
				reponse+=10;
			}else if(roman.charAt(i)=='V'){
				reponse+=5;
			}else if(roman.charAt(i)=='I'){
				reponse+=1;
			}
		}
		System.out.println(reponse);
		return ""+reponse;
	}

}
